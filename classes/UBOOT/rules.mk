###############################################################################
## @file classes/UBOOT/rules.mk
## @author Ricardo Mendoza
## @date 2016/09/28
##
## Rules for UBOOT modules.
###############################################################################

_module_msg := $(if $(_mode_host),Host )U-Boot

include $(BUILD_SYSTEM)/classes/GENERIC/rules.mk

###############################################################################
###############################################################################

# Linux configuration file or target
UBOOT_CONFIG_FILE := $(call module-get-config,$(LOCAL_MODULE))

###############################################################################
###############################################################################

# Use .config provided
define uboot-setup-config
	@mkdir -p $(UBOOT_BUILD_DIR)
	$(Q) cp -af $(UBOOT_CONFIG_FILE) $(UBOOT_BUILD_DIR)/.config
endef

# Rule to create .config
$(UBOOT_BUILD_DIR)/.config: $(UBOOT_CONFIG_FILE)
	+$(uboot-setup-config)


###############################################################################
###############################################################################

# Avoid compiling kernel at same time than header installation by adding a prerequisite
$(UBOOT_BUILD_DIR)/$(LOCAL_MODULE_FILENAME): $(UBOOT_BUILD_DIR)/.config 
	@echo "Checking U-Boot config: $(UBOOT_CONFIG_FILE)"
	$(Q) yes "" 2>/dev/null | $(MAKE) $(UBOOT_MAKE_ARGS) oldconfig
	@echo "Building U-Boot"
	
	$(Q) $(MAKE) $(UBOOT_MAKE_ARGS)
	
	@mkdir -p $(TARGET_OUT_STAGING)/boot
	@cp -af $(UBOOT_BUILD_DIR)/u-boot.bin $(TARGET_OUT_STAGING)/boot; \

	@echo "U-Boot built" 
	@touch $@

###############################################################################
###############################################################################

# Custom clean rule. LOCAL_MODULE_FILENAME already deleted by common rule
# make clean may fail, so ignore its error
.PHONY: uboot-clean
uboot-clean:
	$(Q) if [ -d $(UBOOT_BUILD_DIR) ]; then \
		$(MAKE) $(UBOOT_MAKE_ARGS) --ignore-errors \
			clean || echo "Ignoring clean errors"; \
	fi
